from django.shortcuts import render, HttpResponse
from .models import *

# Create your views here.
def index(request):
	grupo1 = Estudiante.objects.filter(grupo=1)
	grupo2 = Estudiante.objects.filter(grupo=4)
	apell = Estudiante.objects.filter(apellidos="Garces") | Estudiante.objects.filter(apellidos="Gómez") | Estudiante.objects.filter(apellidos="Landaverde") | Estudiante.objects.filter(apellidos="Mendoza")
	samedad = Estudiante.objects.filter(edad=20) | Estudiante.objects.filter(edad=21) | Estudiante.objects.filter(edad=22) | Estudiante.objects.filter(edad=23) | Estudiante.objects.filter(edad=24) | Estudiante.objects.filter(edad=25)
	grupo3 = Estudiante.objects.filter(grupo=3,edad=22)
	todosE = Estudiante.objects.all()

	return render(request, 'index.html', {'grupo1':grupo1,'grupo2':grupo2,'apell':apell,'samedad':samedad, 'grupo3':grupo3, 'todosE':todosE})